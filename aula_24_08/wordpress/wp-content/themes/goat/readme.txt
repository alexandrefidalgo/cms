=== GOAT ===
Contributors: poena
Requires at least: 4.7
Tested up to: 5.4-alpha-47069
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Requires PHP: 5.2
Stable tag: 0.6

GOAT is a basic responsive sports theme for SportsPress.

== Description ==
GOAT is a basic responsive sports theme in black and white with a 
customizable accent color so that you can match your team colors. 
The theme has a top navigation bar, a social menu, a two column grid layout, 
and room for a front page header image. 
It has a footer widget area with room for all your widgets (including two custom widgets), 
and one extra front page widget area below the header. 
GOAT also has support for full width content, video header and logo. 
The theme is designed for use together with the SportsPress plugin, 
but works well as a blog even without the plugin. 
The theme is also compatible with WooCommerce and Jetpack.

GOAT is a child theme of Embla.


For Seb <3

GOAT WordPress Theme, Copyright Carolina Nymark 2018-2019.
GOAT is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Change log ==

Version 0.5, 2020-02-12
Made the theme compatible with a new option added to the parent theme.
This options lets you show the featured image as a header image on posts and pages.

Version 0.5 2019-07-19
Added the accessibility-ready tag. (The final review can be found here https://themes.trac.wordpress.org/ticket/69931)
Fixed a problem with the bottom border of the links in the main menu.
Minor code style changes to better match coding standards.

Version 0.4 2019-06-08
Updated the style of the submit buttons to use the accent color.
Added support for wp_body_open.
Added role="main" to the main section in sportspress.php.

Version 0.3 2019-03-09
Removed parent theme customizer option.
Added GPL license file.

Version 0.2 2019-02-26
A minor style change to match the new menu options added to the parent theme.
Updated readme file.
Added links to the themes support and rating page.



== Credits ==
* Based on Embla https://wordpress.org/themes/embla/  (C) 2017-2020 Carolina Nymark, [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html) 
* Based on Farm https://wordpress.org/themes/farm/  (C) 2018-2020 Carolina Nymark, [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html) 
* Based on Underscores http://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* Based on Twenty Seventeen WordPress Theme, https://wordpress.org/themes/twentyseventeen/, Copyright 2016-2020 WordPress.org, [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

 * Checkbox sanitization Copyright (c) 2015, WordPress Theme Review Team, [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
  https://github.com/WPTRT/code-examples/blob/master/customizer/sanitization-callbacks.php 

* Font Awesome icons, http://fontawesome.io/  [Creative Commons Attribution 4.0] (https://creativecommons.org/licenses/by/4.0/).
* Dashicons, Copyright 2015 WordPress.org. Dashicons is licensed under GPLv2, or any later version with font exception. https://github.com/WordPress/dashicons
* TGM-Plugin-Activation Copyright (c) 2011, Thomas Griffin, license [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

== Asssets ==
The photo is public domain, CC0.
The original photo can be download from:
https://pixabay.com/en/the-ball-stadion-football-the-pitch-488700/