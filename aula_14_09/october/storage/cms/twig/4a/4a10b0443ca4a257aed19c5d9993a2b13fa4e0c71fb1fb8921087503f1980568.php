<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\xampp\htdocs\cms\october/themes/hambern-hambern-blank-bootstrap-4/pages/noticias.htm */
class __TwigTemplate_66090ae907291131d1054a5ec498573399cd70e2509c1c22852ff14a6b722264 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = array("put" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['put'],
                [],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('jumbotron'        );
        // line 2
        echo "  <h1 class=\"display-3\">Notícias do seu time</h1>
  <p class=\"lead\">Brasileirão 2020</p>
  <p>Acesse a página <a href=\"https://www.google.com/search?gs_ssp=eJzj4tTP1TdIy8s2LzRg9OJOKkoszsxJzSxKzAcAXOUICQ&q=brasileirao&rlz=1C1EJFC_enBR878BR878&oq=bras&aqs=chrome.2.69i57j35i39j46l2j0j69i60l3.5751j0j9&sourceid=chrome&ie=UTF-8#sie=lg;/g/11fmzksb3y;2;/m/0fnk7q;nw;fp;1;;\">Google esporte</a></p>
";
        // line 1
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\cms\\october/themes/hambern-hambern-blank-bootstrap-4/pages/noticias.htm";
    }

    public function getDebugInfo()
    {
        return array (  69 => 1,  64 => 2,  62 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% put jumbotron %}
  <h1 class=\"display-3\">Notícias do seu time</h1>
  <p class=\"lead\">Brasileirão 2020</p>
  <p>Acesse a página <a href=\"https://www.google.com/search?gs_ssp=eJzj4tTP1TdIy8s2LzRg9OJOKkoszsxJzSxKzAcAXOUICQ&q=brasileirao&rlz=1C1EJFC_enBR878BR878&oq=bras&aqs=chrome.2.69i57j35i39j46l2j0j69i60l3.5751j0j9&sourceid=chrome&ie=UTF-8#sie=lg;/g/11fmzksb3y;2;/m/0fnk7q;nw;fp;1;;\">Google esporte</a></p>
{% endput %}", "C:\\xampp\\htdocs\\cms\\october/themes/hambern-hambern-blank-bootstrap-4/pages/noticias.htm", "");
    }
}
